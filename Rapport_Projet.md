<h1><center> Informatique industrielle - Miniprojet</center></h1>

<h2><center> Sébastien Lavielle - Laura Gee</center></h2>



<h3><strong>Introduction</strong></h3>
<p>L'objectif de notre projet est de connecter deux cartes par liaison série afin de jouer à un jeu de dominos : chaque joueur possède 4 cartes qu'il peut placer grâce à l'écran tactile, et le premier à se débarasser de ses 4 cartes a gagné. Chaque domino doit partager le même chiffre que son voisin. S'il n'est pas possible pour le joueur de placer un domino, il peut piocher grâce à un bouton. De plus, un deuxième bouton permet de faire pivoter le domino que l'on tient, et le bouton du joystick permet de poser le domino, et d'envoyer sa position ainsi que ses nombres par liaison série à l'autre carte. </p>

<img src=Partie.jpg>

<p>La partie est censée se terminer lorsqu'un joueur a pu placer toutes ses cartes. Cependant, nous n'avons pas eu le temps de coder une tâche de fin de partie car nous avons été retardés par des difficultés au niveau de la liaison série entre les deux cartes. </p>

<h3><strong>Les différentes tâches et leurs interactions</strong></h3>

<p>Le schéma synoptique ci-dessous décrit le fonctionnement global du système.</p>

<img src=Syno.jpg>

<p>On a donc une grosse tâche principale d'affichage des dominos sélectionnés. Elle se base sur la position des doigts sur l'écran tactile pour sélectionner un domino du jeu et le déplacer, ainsi que sur les états des boutons poussoirs pour modifier l'orientation (fonction <em>turn</em>), lancer une pioche ou finir le tour en déposant le domino. La pioche consiste juste à relancer l'une des tâches 'Jeu' qui gèrent la main du joueur et sont suspendues lorsque l'on sélectionne le domino correspondant.</p> 

<p>On utilise deux fonctions pour l'affichage, <em>draw</em> et <em>turn</em> qui permettent en fonction des coordonnées et des numéros sur les dominos de les dessiner à l'écran. Bien entendu, un *Semaphore} permet de gérer l'accès à l'écran par les différentes tâches, mais celui-ci n'apparaît pas sur le schéma pour ne pas le surcharger.</p>

<h4><strong>Fonctions <em>draw</em> et <em>turn</em></strong></h4>

<p>La fonction <em>draw</em> affiche un des deux carrés composant le domino. On dessine dans un premier temps le contour à la position <em>(x,y)</em> souhaitée, puis on effectue un switch sur le numéro à écrire, afin de dessiner le bon nombre de points :</p>

<p>La variable <em>u</em> permet de choisir le mode afficher ou effacer le carré. Le deuxième carré est dessiné par la fonction turn, qui s'appuie sur le bouton poussoir 1 pour gérer une machine à états. Chaque état correspond à une position du carré 2 par rapport au carré 1. Finalement la combinaison des deux fonctions permet de dessiner un domino entier et orientable.</p>

<h4><strong>Génération aléatoire des numéros</strong></h4>

<p>Le premier jeu distribué (4 tâches, une pour chaque domino de la main) est aléatoire. On utilise pour générer des nombres aléatoires les fonctions fournies par FreeRTOS.</p>

`HAL_RNG_GenerateRandomNumber(&hrng, &n1);`

La ligne de code précédente permet d'écrire à l'adresse de la variable $n1$ un nombre aléatoire composé d'une succession de 32 bits aléatoires (entre 0 et 4284867296). On doit ensuite s'assurer que les nombres générés sont bien entre 1 et 6 pour les afficher sur les dominos. Cela se fait par une boucle *while* qui génère les nombres tant qu'ils ne sont pas entre 1 et 6. 

`while ((n2 > 6) || (n2 < 1) || (m2 > 6) || (m2 < 1) || (s2 == 1)) {`</p>
`	HAL_RNG_GenerateRandomNumber(&hrng, &n2);`</p>
`	HAL_RNG_GenerateRandomNumber(&hrng, &m2);}`</p>

La solution est loin d'être optimale mais c'est la plus simple à mettre en oeuvre. Une fois les nombres générés, on les note sur le blackboard afin de pouvoir les utiliser facilement dans la tâche d'affichage.
Attention, pour utiliser la génération aléatoire de nombres, il faut avoir activé la génération aléatoire dans l'IOC.

<img src=RNG.PNG>

## Sélection et affichage du domino
On utilise la fonction de l'écran tactile pour sélectionner et afficher un domino. La main est affichée en bas de l'écran. Si la position verticale du doigt sur l'écran tactile est en dessous d'un certain seuil, on teste sa position horizontale, et si celle-ci coïncide avec un domino du jeu, on tue la tâche correspondante et on efface le domino correspondant, puis on lit les nombres sur le blackboard.  Exemple pour le domino 1 :

`if ((ydo > 200) && (ydo < 260)) {`</p>
		`if ((xdo > 70) && (xdo < 90)) {`</p>
			`vTaskSuspend(Jeu1Handle);`</p>
			`taskENTER_CRITICAL();`</p>
			`n = i1;`</p>
			`m = j1;`</p>
			`taskEXIT_CRITICAL();`</p>
			`BSP_LCD_SetTextColor(LCD_COLOR_WHITE);`</p>
			`BSP_LCD_FillRect(80, 220, 25, 45);`</p>

Ensuite, une fois qu'on a les bons numéros, on peut déplacer le domino sur l'écran tactile :  

`draw(n, xdo, ydo, 1);`</p>
`turn(m, xdo, ydo);`</p>
`vTaskDelay(50);`</p>
`draw(n, xdo, ydo, 0);`</p>
`turn(m, xdo, ydo);`</p>
`draw(m, xdo, ydo + 20, 0);`</p>
`draw(m, xdo, ydo - 20, 0);`</p>
`draw(m, xdo + 20, ydo, 0);`</p>
`draw(m, xdo - 20, ydo, 0);`

Lorsque le dernier argument de draw est à 0, on efface le domino, ainsi à chaque relance de la tâche on efface la position antérieure, ce qui permet un déplacement fluide. De plus, étant donné que le deuxième carré peut être dans 4 positions différentes grâce à la fonction *turn*, on efface tous les alentours du premier carré. Là encore, c'est une solution de facilité que l'on choisit, car les fonctions *turn* et *draw* n'ont pas été écrites par la même personne.


## Liaison série entre les deux cartes
On cherche à envoyer, sur interruption, la position du domino placé par l'autre joueur, pour pouvoir communiquer entre les deux cartes. Pour cela, l'idée est d'envoyer un buffer lorsqu'un joueur finit son tour (par un appui sur le joystick), et de suspendre la tâche d'affichage jusqu'à ce que l'autre joueur ait joué. Ainsi les deux joueurs ont le même code et se renvoient le tour à chaque appui sur le joystick, via la liaison série.</p>
### Connectique
Pour connecter les deux cartes, on doit bien faire attention au circuit imprimé, pour bien connecter la patte de transmission d'une carte à la patte de réception de l'autre, et se placer sur les bonnes pattes d'horloge, sinon la liaison ne pourra pas fonctionner : 

<img src=Fils.jpg>


 ### Code
Dans un premier temps, nous voulions envoyer un message à la tâche *Adversaire* depuis l'interruption à la réception du buffer sur la liaison série, comme dans le TP3. </p>
`void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){`</p>
`	uint16_t Message[4];`</p>
`	Message[0] = rxbuffer[0];`</p>
`	Message[1] = rxbuffer[1];`</p>
`	Message[2] = rxbuffer[2];`</p>
`	Message[3] = rxbuffer[3];`</p>
`	xQueueSendFromISR(QadvHandle, &Message, 0);`</p>
 `   HAL_UART_Receive_IT(&huart1,rxbuffer,1);`
`}`

Cependant, l'utilisation des messages n'a pas fonctionné directement (lors de la réception du message, seule la première composante *Message[0]* correspondait à celle envoyée, et les autres étaient changées. Nous avons donc opté pour l'utilisation du blackboard pour lire les données transmises par l'autre carte :

`if ((rxbuffer[2] != 0)&(rxbuffer[3] != 0)){`</p>
`	taskENTER_CRITICAL();`</p>
`	advx = rxbuffer[0];`</p>
`	advy = rxbuffer[1];`</p>
`	advn = rxbuffer[2];`</p>
`	advm = rxbuffer[3];`</p>
`	taskEXIT_CRITICAL();}`

La condition de report des données reçues sur le tableau permet de s'assurer qu'il n'y a pas d'erreur dans la transmission des numéros écrits sur les dominos. On a en effet pu observer des cas où les composantes du buffer étaient toutes à 0, ce qui est impossible dans le cas des composantes 2 et 3 qui correspondent aux numéros sur les dominos, donc doivent être comprises entre 1 et 6. 



# Conclusion

En conclusion, à travers ce projet nous avons pu revoir les notions de priorités et de gestions des tâches notamment par des actions de suspension et de relance entre elles, ainsi que la gestion de l'écran par un Semaphore partagé. De plus nous avons pu utiliser la génération aléatoire de nombres proposée par la carte,  ainsi que les différents GPIO, en particulier les boutons poussoirs.

Enfin nous avons pu mettre en oeuvre une liaison série entre deux cartes qui permet d'envoyer des buffers de données d'une carte à l'autre à travers une connexion filaire.

